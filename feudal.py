#!/usr/bin/env python

from __future__ import print_function

import argparse, os, sys, re
import gzip

class Dynasty(object):
	''' a container for PFAM clans '''
	def __init__(self, name='Lowborn'):
		self.name = name
		self.titles = set()

	def grant(self, demesne):
		self.titles.add(demesne)

	def __hash__(self):
		return hash(self.name)

	def __repr__(self):
		return 'Dynasty(\'{}\')'.format(self.name)

class Realm(set):
	''' a container for domains '''
	pass

class Castle(object):
	''' a fortified structure whence to survey domains '''
	pass

class Liege(object):
	''' the controller of a domain '''
	pass

class Count(Liege):
	''' perform counting operations on a domain '''
	pass

class Duke(Liege):
	''' gather counts together to do analyses '''
	pass

class King(Liege):
	''' gather analyses together to do meta-analyses '''
	pass

class Emperor(Liege):
	''' gather analyses and meta-analyses to present fully-clothed information '''
	pass

class Holding(object):
	''' a PFAM seed sequence '''
	def __init__(self, id, demesne=None, sequence=''):
		self.id = id
		self.demesne = demesne
		self.sequence = sequence

	def get_demesne(self):
		if demesne is None: raise ValueError('Holding {} has no parent'.format(self.id))
		else: return self.demesne

	def get_parent(self): return self.get_demesne()

	def __hash__(self):
		return hash((self.id, self.sequence))

class Demesne(object):
	''' a domain, as in one from PFAM '''
	def __init__(self, id, dynasty='Unclaimed', name='', abbreviation='', description=''):
		self.id = id
		self.dynasty = dynasty
		self.name = name
		self.abbreviation = abbreviation
		if self.abbreviation and not self.name: self.name = self.abbreviation
		elif self.name and not self.abbreviation: self.abbreviation = self.name
		self.description = description

		self.holdings = []

	def __hash__(self):
		return hash((self.id, self.name, self.abbreviation, self.description))

	def __repr__(self):
		return 'Demesne(id={}, dynasty={}, abbr={})'.format(self.id, self.dynasty, self.abbreviation)

class Church(object):
	''' what maintains domains and bridges RAM and disk '''
	def __init__(self):
		self.clans = set()
		self.realm = Realm()
		self.titles = dict()

	def get_demesne_by_abbreviation(self, abbreviation):
		for demesne in self.realm:
			if demesne.abbreviation == abbreviation: return demesne
		return None

	@staticmethod
	def parse_file(f):
		selfobj = Church()

		faithful_dynasties = {}
		for l in f:
			records = l.strip().split('\t')
			dynasty = None
			try: dynasty = faithful_dynasties[records[1]]
			except KeyError:
				dynasty_name = records[1] if records[1] else 'Lowborn'
				dynasty = Dynasty(name=dynasty_name)
				selfobj.clans.add(dynasty)
				faithful_dynasties[records[1]] = dynasty
			dynasty = Dynasty() if dynasty is None else dynasty
			

			demesne = Demesne(id=records[0], dynasty=dynasty, name=records[2], abbreviation=records[3], description=records[4])
			selfobj.realm.add(demesne)
			dynasty.grant(demesne)

		return selfobj

	def get_owner(self, domain):
		if type(domain) is str:
			for p in self.realm:
				if p.name == domain: return p.dynasty
				elif p.id == domain: return p.dynasty
				elif p.abbreviation == domain: return p.dynasty
		else:
			for p in self.realm:
				if p.name == domain.name: return p.dynasty
		return Dynasty()

	def __hash__(self):
		return hash((self.clans, self.realm, tuple(sorted(self.titles.keys())), tuple(sorted(self.titles.values()))))
			

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('command')
	parser.add_argument('pargs', nargs='*')
	
	if 'PFAMCLANSDB' in os.environ: parser.add_argument('--pfamclansdb', default=os.environ['PFAMCLANSDB'], help='PFAM clan table')
	else: parser.add_argument('--pfamclansdb', default='Pfam-A.clans.tsv.gz', help='PFAM clan table')

	if 'PFAMDB' in os.environ: parser.add_argument('--pfamdb', default=os.environ['PFAMDB'], help='PFAM database')
	else: parser.add_argument('--pfamdb', default='Pfam-A.hmm', help='PFAM database')

	args = parser.parse_args()

	def usage(code=1):
		parser.print_usage()
		exit(code)
	#TODO: autodetect gz, if any
	dbf = gzip.GzipFile(args.pfamclansdb)
	church = Church.parse_file(dbf)

	if args.command == 'help': 
		parser.print_help()
		exit(0)
	elif args.command == 'list':
		if len(args.pargs) < 2: usage()
		inf = open(args.pargs[1])
		#items = search(inf, db=dbf, level=args.pargs[0])
			
		realm = Realm()
		for l in inf:
			if not l.strip(): continue
			elif l.startswith('#'): continue
			else: realm.add(l.strip().split()[0])
			
		if args.pargs[0].startswith('uniq'): [print(domain) for domain in sorted(realm)]
		elif args.pargs[0].startswith('clan'): 
			#[print(domain) for domain in sorted(realm)]
			for domain in sorted(realm):
				ruler = church.get_owner(domain)
				if ruler.name == 'Lowborn': continue
				print(ruler.name)
				#print(ruler)
		elif args.pargs[0].startswith('neighbor'):
			dynasties = []
			for domain in sorted(realm):
				dynasty = church.get_owner(domain)
				if dynasty.name == 'Lowborn': continue
				else: dynasties.append(dynasty)
			subrealm = Realm()

			#ensures that self will always match
			for abbreviation in sorted(realm): 
				demesne = church.get_demesne_by_abbreviation(abbreviation)
				if demesne is None: continue
				subrealm.add(demesne)
				print(demesne)

			for dynasty in dynasties:
				for province in dynasty.titles:
					subrealm.add(province)
					#print(province.name, province)
			#TODO: configure what to output
			#for province in sorted(subrealm): print(province.abbreviation)
			#for province in sorted(subrealm): print(province.id)
			for province in sorted(subrealm): 
				#print(r'{} & {} & {} & {} \\'.format(province.dynasty.name, province.name, province.id, province.abbreviation))
				print('{}\t{}\t{}\t{}'.format(province.dynasty.name, province.name, province.id, province.abbreviation))
				

	else:
		parser.print_usage()
		exit(0)
